﻿using IPZLabrary.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZLabrary.Domain.Repositories
{
    public class BookRepository
    {
        public async Task<IEnumerable<Book>> GetAllAsync()
        {
            using (var bookContext = new LibraryContext())
            {
                return await bookContext.Books.ToListAsync();
            }
        }

        public async Task<Book> GetAsync(int id)
        {
            using (var bookContext = new LibraryContext())
            {
                return await bookContext.Books.FindAsync(id);
            }
        }
    }
}
