﻿using AutoMapper;
using IPZLibrary.Domain.Entities;
using IPZLibrary.BusinessLogic.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZLibrary.BusinessLogic.Infrastructure.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            CreateMap<Book, IndexHomeBookViewModel>();
            CreateMap<IEnumerable<Book>, IndexHomeViewModel>().ForMember(dest => dest.Books,
                opt => opt.ResolveUsing(x => x));
        }
    }
}
