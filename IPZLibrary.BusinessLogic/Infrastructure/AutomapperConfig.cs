﻿using AutoMapper;
using IPZLibrary.BusinessLogic.Infrastructure.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZLibrary.BusinessLogic.Infrastructure
{
    public static class AutomapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<BookProfile>());
        }
    }
}
