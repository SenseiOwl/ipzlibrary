﻿using IPZLibrary.BusinessLogic.ViewModels;
using IPZLibrary.BusinessLogic.ViewModels.Book;
using IPZLibrary.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using IPZLibrary.Domain.Entities;

namespace IPZLibrary.BusinessLogic.Services
{
    public class BookService
    {
        private BookRepository _repository;
        public BookService()
        {
            _repository = new BookRepository();
        }

        public async Task<IndexHomeViewModel> GetAllAsync()
        {
            var books = await _repository.GetAllAsync();
            var result = Mapper.Map<IEnumerable<Book>, IndexHomeViewModel>(books);
            return result;
        }

        public async Task<GetBookBookViewModel> GetBookAsync(int id)
        {
            var book = await _repository.GetAsync(id);
            if(book == null)
            {
                return null;
            }
            var result = Mapper.Map<Book, GetBookBookViewModel>(book);
            return result;
        }
    }
}
