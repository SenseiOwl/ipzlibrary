﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZLabrary.BusinessLogic.ViewModels
{
    public class IndexHomeViewModel
    {
        public ICollection<IndexHomeBookViewModel> Books { get; set; }
        public IndexHomeViewModel()
        {
            Books = new List<IndexHomeBookViewModel>();
        }
    }
}
