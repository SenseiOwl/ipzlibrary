﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZLabrary.BusinessLogic.ViewModels.Book
{
    public class GetBookBookViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public int Edition { get; set; }
        public int YearOfWriting { get; set; }
        public int YearOfPublishig { get; set; }
        public string Genre { get; set; }
        public int PageCount { get; set; }
        public int Description { get; set; }
        public int TotalAmount { get; set; }
        public int InStock { get; set; }
    }
}
