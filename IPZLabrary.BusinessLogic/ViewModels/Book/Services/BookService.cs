﻿using IPZLabrary.BusinessLogic.ViewModels;
using IPZLabrary.BusinessLogic.ViewModels.Book;
using IPZLabrary.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPZLabrary.BusinessLogic.Services
{
    public class BookService
    {
        private BookRepository _repository;
        public BookService()
        {
            _repository = new BookRepository();
        }

        public async Task<IndexHomeViewModel> GetAllAsync()
        {
            var books = await _repository.GetAllAsync();
            var result = new IndexHomeViewModel();
            foreach(var book in books)
            {
                var resultBook = new IndexHomeBookViewModel();
                resultBook.Id = book.Id;
                resultBook.Name = book.Name;
                resultBook.Author = book.Author;
                resultBook.YearOfWriting = book.YearOfWriting;
                result.Books.Add(resultBook);
            }
            return result;
        }

        public async Task<GetBookBookViewModel> GetBookAsync(int id)
        {
            var book = await _repository.GetAsync(id);
            var result = new GetBookBookViewModel();
            if(book == null)
            {
                return null;
            }
            result.Id = book.Id;
            result.Name = book.Name;
            result.Author = book.Author;
            result.Edition = book.Edition;
            result.Genre = book.Genre;
            result.Description = book.Description;
            result.PageCount = book.PageCount;
            result.TotalAmount = book.TotalAmount;
            result.YearOfPublishig = book.YearOfPublishig;
            result.YearOfWriting = book.YearOfWriting;
            result.InStock = book.InStock;

            return result;
        }
    }
}
