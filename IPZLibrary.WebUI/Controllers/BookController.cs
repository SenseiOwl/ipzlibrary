﻿using IPZLibrary.BusinessLogic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IPZLibrary.WebUI.Controllers
{
    public class BookController : Controller
    {
        private BookService _service;

        public BookController()
        {
            _service = new BookService();
        }
        public async Task<ActionResult> GetBook(int id)
        {
            var viewModel = await _service.GetBookAsync(id);
            return View(viewModel);
        }

        public async Task<ActionResult> GetAll()
        {
            var viewModel = await _service.GetAllAsync();
            return View(viewModel);
        }

    }
}