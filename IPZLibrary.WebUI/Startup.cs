﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IPZLibrary.WebUI.Startup))]
namespace IPZLibrary.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
