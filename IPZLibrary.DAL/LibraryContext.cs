﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using IPZLibrary.Domain.Entities;

namespace IPZLibrary.Domain
{
    public class LibraryContext : DbContext
    {
        public LibraryContext() : base("LibraryConnection")
        {

        }
        public DbSet<Book> Books { get; set; } 
    }
}
